﻿using System;
using System.Collections;
using UnityEngine;


public class ClickTrigger : MonoBehaviour
{
    public UnityEngine.Object Door;
    private GameObject targetGameObject = null;
    public int Distance;
    public float timespan;

    private bool open = false;
    private float degrees = 0;
    private float _rotated = 0;
    private Vector3 axis = new Vector3(0, 1, 0);
    private Vector3 _rotationVector;


    void Start()
    {
        targetGameObject = Door as GameObject;

        axis.Normalize();
        _rotationVector = axis * degrees;

    }
    void Update()
    {
        var rotation = (degrees / timespan) * Time.deltaTime;    
        _rotated += rotation;

        if ( Math.Abs(degrees) > Math.Abs(_rotated))
        {
            targetGameObject.transform.Rotate(0, rotation, 0, Space.Self);
        }
    }

    private void DoActivateTrigger()
    {
        if(targetGameObject != null)
        {
            open = !open;
            _rotated = 0;
            if (open)
            {
                Debug.Log("open");
                degrees = 30;
            }
            else
            {
                degrees = -30;
            }
        }
    }

    private void OnMouseDown()
    {
        if (Vector3.Distance(Camera.main.gameObject.transform.position, gameObject.transform.position) <= Distance)
        {
            DoActivateTrigger();
        }
        
    }
}
