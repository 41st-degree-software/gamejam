﻿using UnityEngine;
using System.Collections;

public class ToggleTrigger : MonoBehaviour {

    public Object target;                       // The game object to affect. If none, the trigger work on this game object

    private void DoActivateTrigger()
    {

        Object currentTarget = target ?? gameObject;
        Behaviour targetBehaviour = currentTarget as Behaviour;
        GameObject targetGameObject = currentTarget as GameObject;
        if (targetBehaviour != null)
        {
            targetGameObject = targetBehaviour.gameObject;
        }

        if (targetGameObject != null)
        {
            targetGameObject.SetActive(!targetGameObject.activeSelf);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        DoActivateTrigger();
    }
}
