﻿using UnityEngine;
using System.Collections;

public class NetworkManager : Photon.PunBehaviour {

	// Use this for initialization
	void Start () {
        //Debug.Log("Start");
        PhotonNetwork.logLevel = PhotonLogLevel.ErrorsOnly;
        Connect();
	}

    void Connect()
    {
        //Debug.Log("Connect");
        PhotonNetwork.ConnectUsingSettings("v1.0.0");

    }

    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        Debug.Log("OnConnectedToMaster");

        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();

        Debug.Log("OnJoinedLobby");

        PhotonNetwork.JoinOrCreateRoom("Room", new RoomOptions(), TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedroom");
        SpawnMyPlayer();
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("OnCreatedroom");
        SpawnMyPlayer();
    }

    void SpawnMyPlayer()
    {
        Debug.Log("SpawnPlayer");

        PhotonNetwork.Instantiate("FPSController", new Vector3(10, 10, 10), Quaternion.identity, 0);
    }
}
